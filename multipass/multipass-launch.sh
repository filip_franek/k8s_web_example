#!/usr/bin/env bash

# This script launches Multipass VM, setups single k8s cluster over MicroK8s 
# and bootstraps Flux with GitLab repo using gitlab token which then syncs 
# k8s manifests to the VM.
# It expects gitlab token to be passed to it or it will read GITLAB_TOKEN from env.
# Usage:
#   ./multipass-launch.sh
#   ./multipass-launch.sh --gitlab-token "your_gitlab_token_here"
#   ./multipass-launch.sh -t "your_gitlab_token_here"
#   GITLAB_TOKEN="your_gitlab_token_here" ./multipass-launch.sh

set -euxo pipefail

# Function to clean up temporary files
cleanup() {
  rm -f "$TMP_YAML"
}

# Register the cleanup function to be called on the EXIT signal
trap cleanup EXIT

# Initialize TOKEN variable
TOKEN=""

# Handle short and long form arguments
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -t|--gitlab-token)
      TOKEN="$2"
      shift # past argument
      shift # past value
      ;;
    *) # unknown option
      shift # past argument
      ;;
  esac
done

# If no token is provided through arguments, use the environment variable
if [ -z "$TOKEN" ]; then
  if [ ! -z "${GITLAB_TOKEN:-}" ]; then
    TOKEN="$GITLAB_TOKEN"
  else
    echo "No GitLab token provided. Exiting."
    exit 1
  fi
fi

# Create a temporary YAML configuration file
TMP_YAML=$(mktemp multipass/temp_config_XXX.yaml)

# Replace the GitLab token placeholder in the YAML configuration file
sed "s/__GITLAB_TOKEN__/$TOKEN/g" multipass/microk8s-config.yaml > "$TMP_YAML"

# Launch the Multipass VM with the modified YAML configuration file
multipass launch lunar -c 2 -m 2G -d 10G --name microk8s-flux-vm --cloud-init "$TMP_YAML"
