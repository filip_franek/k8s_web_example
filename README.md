This is an example project to test deploying web services to Kubernetes.

Initially I took inspiration from  LinkedIn course [Kubernetes Essential Training](https://www.linkedin.com/learning/kubernetes-essential-training-application-development/): Application Development by Matt Turner.

# k8s cluster

You can run it in single cluster on local machine using:
- minikube
- microk8s
- microk8s over multipass (VM)

### minikube
```bash
minikube start
minikube dashboard
minikube stop
```

Then you have to apply the manifest with `kubectl`, but rather use [GitOps](#gitops) approach.

### microk8s
```bash
sudo snap install microk8s --classic
```

**Add your user to microk8s group and add privileges to kube config**
```bash
sudo usermod -a -G microk8s $USER
mkdir ~/.kube
sudo chown -R $USER ~/.kube
newgrp microk8s
# Re-enter the shell session for the group update to take place
su - $USER # or bash
# Alias kubectl for MicroK8s
sudo snap alias microk8s.kubectl kubectl
sudo snap alias microk8s.kubectl k
```

Similarly, you can apply the manifest with `kubectl`, but rather use [GitOps](#gitops) approach.

### microk8s in VM over multipass

Mutlipass is setup with cloud-init config to install microk8s and flux in order to sync and deploy the k8s manifest for the project to the cluster. Flux needs your GitLab token which can be either passed to the `multipass-launch.sh` script or exported as `GITLAB_TOKEN` env variable.

```bash
./multipass/multipass-launch.sh --gitlab-token "your_gitlab_token_here"

# Get IP address of the VM
multipass list

# Shell into VM
multipass shell microk8s-flux-vm

# Check status of microk8s
microk8s status --wait-ready

# Delete VM
multipass delete --purge microk8s-vm
# --purge ensures that VM is removed immediately, without keeping it in deleted state
```

## GitOps

Flux is used to keep k8s cluster in sync with GitLab repo

### Flux

#### GitLab Repo Authentication

Authenticate this repo so Flux. Flux will use the authentication token to read manifests from the repo and commit flux-system manifests.

Create Personal Access Token (PAT):
- url: https://gitlab.com/-/profile/personal_access_tokens
- name: kubernetes-flux-token
- scope: api, read_repository, write_repository

```bash
# The space before the command makes sure it is not saved in bash history
 export GITLAB_TOKEN=<kubernetes-flux-token>
```
#### Bootstrap Flux
```bash
flux bootstrap gitlab \
  --owner=k8s_playground \
  --repository=k8s-web-example \
  --branch=main \
  --path=manifests \
  --token-auth \
  --components-extra=image-reflector-controller,image-automation-controller
```

After bootstrapping, all manifests in the `manifest` folder of this repo should be applied.